"use strict"

 
// function createNewUser() {
//   const newUser = {};

//   // Перевіряємо ім'я, поки не буде введене вірне значення
//   while (true) {
//     const firstName = prompt('Введіть ваше ім\'я:');

//     if (firstName !== null && firstName.trim() !== "" && !/\d/.test(firstName)) {
//       newUser.firstName = firstName;
//       break; // Виходимо з циклу, якщо отримали правильне значення
//     } else {
//       alert("Будь ласка, введіть вірне ім'я (без цифр)!");
//     }
//   }

//   // Перевіряємо прізвище, поки не буде введене вірне значення
//   while (true) {
//     const lastName = prompt('Введіть ваше прізвище:');

//     if (lastName !== null && lastName.trim() !== "" && !/\d/.test(lastName)) {
//       newUser.lastName = lastName;
//       break; // Виходимо з циклу, якщо отримали правильне значення
//     } else {
//       alert("Будь ласка, введіть вірне прізвище (без цифр)!");
//     }
//   }

//   // Запитуємо дату народження та перевіряємо її правильність
//   while (true) {
//     const birthdayString = prompt('Введіть вашу дату народження (у форматі dd.mm.yyyy):');

//     if (/^\d{2}\.\d{2}\.\d{4}$/.test(birthdayString)) {
//       newUser.birthday = birthdayString;
//       break; // Виходимо з циклу, якщо отримали правильне значення
//     } else {
//       alert("Будь ласка, введіть коректну дату народження (у форматі dd.mm.yyyy)!");
//     }
//   }

//   // Метод для отримання логіну
//   newUser.getLogin = function () {
//     return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
//   };

//   // Функція-сеттер для зміни імені
//   newUser.setFirstName = function (newFirstName) {
//     this.firstName = newFirstName;
//   };

//   // Функція-сеттер для зміни прізвища
//   newUser.setLastName = function (newLastName) {
//     this.lastName = newLastName;
//   };

//   // Метод для отримання віку користувача
//   newUser.getAge = function () {
//     const today = new Date();
//     const birthdayParts = this.birthday.split('.');
//     const birthdayDate = new Date(
//       parseInt(birthdayParts[2]),
//       parseInt(birthdayParts[1]) - 1,
//       parseInt(birthdayParts[0])
//     );
//     let age = today.getFullYear() - birthdayDate.getFullYear();
//     const monthDiff = today.getMonth() - birthdayDate.getMonth();
//     if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthdayDate.getDate())) {
//       age--;
//     }
//     return age;
//   };

//   // Метод для отримання паролю
//   newUser.getPassword = function () {
//     const firstInitial = this.firstName[0].toUpperCase();
//     const lastNameLower = this.lastName.toLowerCase();
//     const birthYear = this.birthday.split('.')[2];
//     return `${firstInitial}${lastNameLower}${birthYear}`;
//   };

//   return newUser;
// }

// // Викликати функцію createNewUser() та отримати нового юзера
// const user = createNewUser();

// // Викликати метод getLogin() та вивести результат у консоль
// console.log(user.getLogin());

// // Вивести вік користувача
// console.log(user.getAge());

// // Вивести пароль користувача
// console.log(user.getPassword());


function createNewUser() {
  let question = prompt("enter your birthday  dd.mm.yyyy");
  let firstName = prompt("enter first name:");
  let secondName = prompt("enter second name");

  while (
    !firstName ||
    !isNaN(firstName) ||
    firstName.trim() === "" ||
    !secondName ||
    !isNaN(secondName) ||
    secondName.trim() === "" || !question ||  question.trim() === ""
  ) {
    firstName = prompt("enter first name:");
    secondName = prompt("enter second name");
    question = prompt("enter your birthday  dd.mm.yyyy");
  }

  const newUser = {
    firstname: firstName,
    lastname: secondName,
    birthday: question,
    getPassword(){
      return this.firstname[0].toUpperCase() + this.lastname.toLowerCase() + this.birthday.substring(6,10);
    },

  

    getAge(){
      const dateNow = new Date();
      const birthdayDay = +this.birthday.substring(0, 2);
      const birthdayMonth = +this.birthday.substring(3, 5);
      const birthdayYear = +this.birthday.substring(6, 10);
      const birthday = new Date(birthdayYear, birthdayMonth, birthdayDay);
      let result = dateNow.getFullYear() - birthdayYear;
      console.log(result, dateNow.getFullYear(), birthdayYear);
      if (birthdayMonth > dateNow.getMonth() + 1){

          result = result - 1;
      }
      return result;

    },



     setFirstName(data) {
      this.firstname = data;
    },
     setLastName(data) {
      this.lastname = data;
    },

    getLogin() {
      let firstAnswer = this.firstname.charAt(0).toLowerCase();
      let secondAnswer = this.lastname.toLowerCase();
      return `${firstAnswer}${secondAnswer}`;
    },
  };

  return newUser;
}



const result = createNewUser();

console.log(result.getAge(), result.getPassword() );



